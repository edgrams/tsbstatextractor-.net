﻿using System;
using NUnit.Framework;
using Rhino.Mocks;
using TSBStatExtractor.Exceptions;
using TSBStatExtractor.Interfaces;

namespace TSBStatExtractor.Tests
{
    [TestFixture]
    public class StatExtractorTests
    {
        private StatExtractor statExtractor;
        private IExtractorFactory extractorFactory;
        private IExtractor extractor;

        [SetUp]
        public void SetUp()
        {
            this.extractorFactory = MockRepository.GenerateMock<IExtractorFactory>();
            this.extractor = MockRepository.GenerateMock<IExtractor>();

            this.statExtractor = new MockStatExtractor(this.extractorFactory);
        }

        [Test]
        public void ExtractFromFile_Should_Throw_When_FilePath_Is_Null()
        {
            Assert.Throws<ArgumentNullException>(() => this.statExtractor.ExtractFromFile(null));
        }

        [Test]
        public void ExtractFromFile_Should_Throw_When_FilePath_Is_Empty()
        {
            Assert.Throws<ArgumentNullException>(() => this.statExtractor.ExtractFromFile(string.Empty));
        }

        [Test]
        public void ExractFromFile_Should_Throw_When_FilePath_Does_Not_Exist()
        {
            Assert.Throws<TSBStatExtractorException>(() => this.statExtractor.ExtractFromFile("C:\\notarealfile.txt"));
        }

        [Test]
        public void Extract_Should_Throw_When_SaveState_Is_Null()
        {
            Assert.Throws<ArgumentNullException>(() => this.statExtractor.Extract(null));
        }

        [Test]
        public void Extract_Should_Request_Build_From_ExtractorFactory()
        {
            var nesticleBytes = new byte[1];
            this.extractorFactory.Expect(p => p.Build(0)).IgnoreArguments().Return(this.extractor);

            this.statExtractor.Extract(nesticleBytes);

            this.extractorFactory.VerifyAllExpectations();
        }

        [Test]
        public void Extract_Should_Request_GetGameStats_From_Extractor()
        {
            var nesticleBytes = new byte[1];
            this.extractorFactory.Stub(p => p.Build(0)).IgnoreArguments().Return(this.extractor);
            extractor.Expect(p => p.GetGameStats(null)).IgnoreArguments().Return(null);

            this.statExtractor.Extract(nesticleBytes);

            extractor.VerifyAllExpectations();
        }

        private class MockStatExtractor : StatExtractor
        {
            private readonly IExtractorFactory extractorFactory;

            public MockStatExtractor(IExtractorFactory extractorFactory)
            {
                this.extractorFactory = extractorFactory;
            }

            internal override void LoadModules()
            {
                // do nothing!
            }

            internal override IExtractorFactory GetExtractorFactory()
            {
                return this.extractorFactory;
            }
        }
    }
}