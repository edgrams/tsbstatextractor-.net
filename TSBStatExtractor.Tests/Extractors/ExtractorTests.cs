﻿using System.Linq;
using NUnit.Framework;
using TSBDomain.Enums;
using TSBStatExtractor.Extractors;
using System.Collections.Generic;
using TSBDomain.Models.Player.Stats;
using TSBStatExtractor.Extractors.TSBOriginal;

namespace TSBStatExtractor.Tests.Extractors
{
    [TestFixture]
    public class ExtractorTests
    {
        [Test]
        public void CreateTeamStats_Should_Return_List_With_One_Home_Team()
        {
            var result = Extractor.CreateTeamStats();

            Assert.AreEqual(1, result.Count(p => p.IsHome));
        }

        [Test]
        public void CreateTeamStats_Should_Return_List_With_One_Away_Team()
        {
            var result = Extractor.CreateTeamStats();

            Assert.AreEqual(1, result.Count(p => !p.IsHome));
        }

        [Test]
        public void GetDecision_Should_Return_Win_When_Score_Is_Greater_Than_OpposingScore()
        {
            var result = Extractor.GetDecision(1, 0);

            Assert.AreEqual(Decision.Win, result);
        }

        [Test]
        public void GetDecision_Should_Return_Loss_When_Score_Is_Less_Than_OpposingScore()
        {
            var result = Extractor.GetDecision(0, 1);

            Assert.AreEqual(Decision.Loss, result);
        }

        [Test]
        public void GetDecision_Should_Return_Tie_When_Score_Is_Equal_To_OpposingScore()
        {
            var result = Extractor.GetDecision(0, 0);

            Assert.AreEqual(Decision.Tie, result);
        }

        [Test]
        public void GetYards_Should_Return_Yards_When_Multiplier_Is_Zero()
        {
            const int yards = 30;
            var result = Extractor.GetYards(yards, 0);

            Assert.AreEqual(yards, result);
        }

        [Test]
        public void GetYards_Should_Return_Yards_Plus_Multiplier_Times_256_When_Multiplier_Is_Less_Than_10()
        {
            const int yards = 30;
            const int multiplier = 1;
            var result = Extractor.GetYards(yards, multiplier);

            const int expected = yards + (multiplier * 256);
            Assert.AreEqual(expected, result);
        }

        [Test]
        public void GetYards_Should_Return_Correct_Negative_Yards_When_Multiplier_Is_More_Than_10()
        {
            const int yards = 250;
            const int multiplier = 255;
            var result = Extractor.GetYards(yards, multiplier);

            const int expected = -6;
            Assert.AreEqual(expected, result);
        }

        [Test]
        public void MapPlayerId_Should_Map_First_Team_Correct()
        {
            const int teamId = 3;
            var playerStats = new List<PlayerStats>();
            playerStats.Add(new PlayerStats());

            var extractor = new TSBOriginalNestopiaExtractor();
            extractor.MapPlayerId(playerStats, teamId);

            var expected = (teamId * 25) + 1;
            Assert.AreEqual(expected, playerStats[0].PlayerId);
        }

        [Test]
        public void MapPlayerId_Should_Map_Last_Team_Correct()
        {
            const int teamId = 3;
            var playerStats = new List<PlayerStats>();
            playerStats.Add(new PlayerStats());
            playerStats.Add(new PlayerStats());
            playerStats.Add(new PlayerStats());

            var extractor = new TSBOriginalNestopiaExtractor();
            extractor.MapPlayerId(playerStats, teamId);

            var expected = (teamId * 25) + 3;
            Assert.AreEqual(expected, playerStats[2].PlayerId);
        }
    }
}