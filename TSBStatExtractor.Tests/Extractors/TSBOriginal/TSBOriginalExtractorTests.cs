﻿using NUnit.Framework;
using TSBStatExtractor.Extractors.TSBOriginal;

namespace TSBStatExtractor.Tests.Extractors.TSBOriginal
{
    [TestFixture]
    class TSBOriginalExtractorTests
    {
        private byte[] scores;

        [SetUp]
        public void SetUp()
        {
            this.SetupScores();
        }

        [Test]
        public void GetScore_Should_Return_Zero_If_Not_IsOvertime()
        {
            var result = TSBOriginalExtractor.GetScore(this.scores, false, 5);

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetScore_Should_Return_First_Quarter_Score_If_IsOvertime()
        {
            // game winning field goal
            this.scores[0] = 3;

            var result = TSBOriginalExtractor.GetScore(this.scores, true, 5);
            Assert.AreEqual(3, result);
        }

        [Test]
        public void GetScore_Should_Return_Final_Score_Subracted_By_First_Quarter_Score_Divided_By_4_Floor_For_Quarter_If_Overtime_Win()
        {
            // game winning field goal
            this.scores[0] = 3;
            this.scores[4] = 26;

            var result = TSBOriginalExtractor.GetScore(this.scores, true, 1);
            Assert.AreEqual(5.75, result);
        }

        [Test]
        public void GetScore_Should_Return_Final_Score_Subracted_By_First_Quarter_Score_Divided_By_4_Floor_For_Quarter_If_Overtime_Loss()
        {
            // loss
            this.scores[0] = 0;

            var result = TSBOriginalExtractor.GetScore(this.scores, true, 1);
            Assert.AreEqual(5.75, result);
        }

        [Test]
        public void GetScore_Should_Return_Score_For_Quarter()
        {
            var result = TSBOriginalExtractor.GetScore(this.scores, false, 1);

            Assert.AreEqual(this.scores[0], result);
        }

        private void SetupScores()
        {
            this.scores = new byte[] {10, 7, 3, 3, 23};
        }
    }
}
