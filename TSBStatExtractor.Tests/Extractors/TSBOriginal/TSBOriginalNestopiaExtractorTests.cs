﻿using System;
using System.IO;
using System.Linq;
using NUnit.Framework;
using TSBDomain.Enums;
using TSBStatExtractor.Exceptions;
using TSBStatExtractor.Extractors.TSBOriginal;
using TSBDomain.Models;
using TSBDomain.Models.Player.Stats;

namespace TSBStatExtractor.Tests.Extractors.TSBOriginal
{
    [TestFixture]
    class TSBOriginalNestopiaExtractorTests
    {
        private TSBOriginalNestopiaExtractor nestopiaExtractor;

        private byte[] saveStateBytes;
        private byte[] overtimeDecisionSaveStateBytes;
        private byte[] overtimeTieSaveStateBytes;

        private const string SaveStateFilePath = "..\\..\\SaveStates\\tpc_original.ns1";
        private const string OvertimeDecisionSaveStateFilePath =
            "..\\..\\SaveStates\\tpc_original_overtime_decision.ns1";
        private const string OvertimeTieSaveStateFilePath =
            "..\\..\\SaveStates\\tpc_original_overtime_tie.ns1";

        [TestFixtureSetUp]
        public void TestFixtureSetUp()
        {
            this.saveStateBytes = GetSaveStateBytes(SaveStateFilePath);
            this.overtimeDecisionSaveStateBytes = GetSaveStateBytes(OvertimeDecisionSaveStateFilePath);
            this.overtimeTieSaveStateBytes = GetSaveStateBytes(OvertimeTieSaveStateFilePath);
        }

        [SetUp]
        public void SetUp()
        {
            this.nestopiaExtractor = new TSBOriginalNestopiaExtractor();
        }

        #region GetGameStats
        [Test]
        public void GetGameStats_Should_Throw_If_SaveState_Is_Null()
        {
            Assert.Throws<ArgumentNullException>(() => this.nestopiaExtractor.GetGameStats(null));
        }

        [Test]
        public void GetGameStats_Should_Throw_If_SaveState_Length_Is_Not_Equal_To_Nestopia_SaveStateLength()
        {
            Assert.Throws<TSBStatExtractorException>(() => this.nestopiaExtractor.GetGameStats(new byte[0]));
        }

        [Test]
        public void GetGameStats_Should_Return_Instance_Of_Game()
        {
            var result = this.nestopiaExtractor.GetGameStats(this.saveStateBytes);

            Assert.IsInstanceOf<Game>(result);
        }
        #endregion

        #region GetPlayerStats - QB Stats
        [Test]
        public void GetPlayerStats_Should_Get_Correct_Attempts_For_Home_QB1()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[0].PassStats.Attempts;

            Assert.AreEqual(9, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Completions_For_Home_QB1()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[0].PassStats.Completions;

            Assert.AreEqual(5, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Pass_Yards_For_Home_QB1()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[0].PassStats.Yards;

            Assert.AreEqual(100, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Touchdowns_For_Home_QB1()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[0].PassStats.Touchdowns;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Interceptions_For_Home_QB1()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[0].PassStats.Interceptions;

            Assert.AreEqual(1, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Rush_Attempts_For_Home_QB1()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[0].RushStats.Attempts;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Rush_Yards_For_Home_QB1()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[0].RushStats.Yards;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Rush_Touchdowns_For_Home_QB1()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[0].RushStats.Touchdowns;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Attempts_For_Home_QB2()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[1].PassStats.Attempts;

            Assert.AreEqual(4, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Completions_For_Home_QB2()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[1].PassStats.Completions;

            Assert.AreEqual(2, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Pass_Yards_For_Home_QB2()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[1].PassStats.Yards;

            Assert.AreEqual(46, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Touchdowns_For_Home_QB2()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[1].PassStats.Touchdowns;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Interceptions_For_Home_QB2()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[1].PassStats.Interceptions;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Rush_Attempts_For_Home_QB2()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[1].RushStats.Attempts;

            Assert.AreEqual(1, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Rush_Yards_For_Home_QB2()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[1].RushStats.Yards;

            Assert.AreEqual(2, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Rush_Touchdowns_For_Home_QB2()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[1].RushStats.Touchdowns;

            Assert.AreEqual(1, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Attempts_For_Away_QB1()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[25].PassStats.Attempts;

            Assert.AreEqual(8, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Completions_For_Away_QB1()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[25].PassStats.Completions;

            Assert.AreEqual(3, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Pass_Yards_For_Away_QB1()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[25].PassStats.Yards;

            Assert.AreEqual(87, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Touchdowns_For_Away_QB1()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[25].PassStats.Touchdowns;

            Assert.AreEqual(1, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Interceptions_For_Away_QB1()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[25].PassStats.Interceptions;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Rush_Attempts_For_Away_QB1()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[25].RushStats.Attempts;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Rush_Yards_For_Away_QB1()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[25].RushStats.Yards;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Rush_Touchdowns_For_Away_QB1()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[25].RushStats.Touchdowns;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Attempts_For_Away_QB2()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[26].PassStats.Attempts;

            Assert.AreEqual(2, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Completions_For_Away_QB2()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[26].PassStats.Completions;

            Assert.AreEqual(1, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Pass_Yards_For_Away_QB2()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[26].PassStats.Yards;

            Assert.AreEqual(7, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Touchdowns_For_Away_QB2()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[26].PassStats.Touchdowns;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Interceptions_For_Away_QB2()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[26].PassStats.Interceptions;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Rush_Attempts_For_Away_QB2()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[26].RushStats.Attempts;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Rush_Yards_For_Away_QB2()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[26].RushStats.Yards;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Rush_Touchdowns_For_Away_QB2()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[26].RushStats.Touchdowns;

            Assert.AreEqual(0, result);
        }
        #endregion

        #region GetPlayerStats - RB Stats
        [Test]
        public void GetPlayerStats_Should_Get_Correct_Rush_Attempts_For_Home_RB1()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[2].RushStats.Attempts;

            Assert.AreEqual(2, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Rush_Yards_For_Home_RB1()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[2].RushStats.Yards;

            Assert.AreEqual(23, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Rush_Touchdowns_For_Home_RB1()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[2].RushStats.Touchdowns;

            Assert.AreEqual(1, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Receiptions_For_Home_RB1()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[2].RecStats.Receptions;

            Assert.AreEqual(1, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Receiving_Yards_For_Home_RB1()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[2].RecStats.Yards;

            Assert.AreEqual(11, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Receiving_Touchdowns_For_Home_RB1()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[2].RecStats.Touchdowns;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Kick_Returns_For_Home_RB1()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[2].KickReturnStats.Returns;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Kick_Return_Yards_For_Home_RB1()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[2].KickReturnStats.Yards;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Kick_Return_Touchdowns_For_Home_RB1()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[2].KickReturnStats.Touchdowns;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Punt_Returns_For_Home_RB1()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[2].PuntReturnStats.Returns;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Punt_Return_Yards_For_Home_RB1()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[2].PuntReturnStats.Yards;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Punt_Return_Touchdowns_For_Home_RB1()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[2].PuntReturnStats.Touchdowns;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Rush_Attempts_For_Home_RB2()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[3].RushStats.Attempts;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Rush_Yards_For_Home_RB2()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[3].RushStats.Yards;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Rush_Touchdowns_For_Home_RB2()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[3].RushStats.Touchdowns;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Receiptions_For_Home_RB2()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[3].RecStats.Receptions;

            Assert.AreEqual(2, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Receiving_Yards_For_Home_RB2()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[3].RecStats.Yards;

            Assert.AreEqual(43, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Receiving_Touchdowns_For_Home_RB2()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[3].RecStats.Touchdowns;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Kick_Returns_For_Home_RB2()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[3].KickReturnStats.Returns;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Kick_Return_Yards_For_Home_RB2()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[3].KickReturnStats.Yards;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Kick_Return_Touchdowns_For_Home_RB2()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[3].KickReturnStats.Touchdowns;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Punt_Returns_For_Home_RB2()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[3].PuntReturnStats.Returns;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Punt_Return_Yards_For_Home_RB2()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[3].PuntReturnStats.Yards;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Punt_Return_Touchdowns_For_Home_RB2()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[3].PuntReturnStats.Touchdowns;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Rush_Attempts_For_Home_RB3()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[4].RushStats.Attempts;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Rush_Yards_For_Home_RB3()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[4].RushStats.Yards;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Rush_Touchdowns_For_Home_RB3()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[4].RushStats.Touchdowns;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Receiptions_For_Home_RB3()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[4].RecStats.Receptions;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Receiving_Yards_For_Home_RB3()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[4].RecStats.Yards;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Receiving_Touchdowns_For_Home_RB3()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[4].RecStats.Touchdowns;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Kick_Returns_For_Home_RB3()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[4].KickReturnStats.Returns;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Kick_Return_Yards_For_Home_RB3()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[4].KickReturnStats.Yards;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Kick_Return_Touchdowns_For_Home_RB3()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[4].KickReturnStats.Touchdowns;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Punt_Returns_For_Home_RB3()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[4].PuntReturnStats.Returns;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Punt_Return_Yards_For_Home_RB3()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[4].PuntReturnStats.Yards;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Punt_Return_Touchdowns_For_Home_RB3()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[4].PuntReturnStats.Touchdowns;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Rush_Attempts_For_Home_RB4()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[5].RushStats.Attempts;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Rush_Yards_For_Home_RB4()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[5].RushStats.Yards;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Rush_Touchdowns_For_Home_RB4()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[5].RushStats.Touchdowns;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Receiptions_For_Home_RB4()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[5].RecStats.Receptions;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Receiving_Yards_For_Home_RB4()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[5].RecStats.Yards;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Receiving_Touchdowns_For_Home_RB4()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[5].RecStats.Touchdowns;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Kick_Returns_For_Home_RB4()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[5].KickReturnStats.Returns;

            Assert.AreEqual(4, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Kick_Return_Yards_For_Home_RB4()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[5].KickReturnStats.Yards;

            Assert.AreEqual(63, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Kick_Return_Touchdowns_For_Home_RB4()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[5].KickReturnStats.Touchdowns;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Punt_Returns_For_Home_RB4()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[5].PuntReturnStats.Returns;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Punt_Return_Yards_For_Home_RB4()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[5].PuntReturnStats.Yards;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Punt_Return_Touchdowns_For_Home_RB4()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[5].PuntReturnStats.Touchdowns;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Rush_Attempts_For_Away_RB1()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[27].RushStats.Attempts;

            Assert.AreEqual(3, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Rush_Yards_For_Away_RB1()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[27].RushStats.Yards;

            Assert.AreEqual(45, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Rush_Touchdowns_For_Away_RB1()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[27].RushStats.Touchdowns;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Receiptions_For_Away_RB1()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[27].RecStats.Receptions;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Receiving_Yards_For_Away_RB1()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[27].RecStats.Yards;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Receiving_Touchdowns_For_Away_RB1()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[27].RecStats.Touchdowns;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Kick_Returns_For_Away_RB1()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[27].KickReturnStats.Returns;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Kick_Return_Yards_For_Away_RB1()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[27].KickReturnStats.Yards;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Kick_Return_Touchdowns_For_Away_RB1()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[27].KickReturnStats.Touchdowns;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Punt_Returns_For_Away_RB1()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[27].PuntReturnStats.Returns;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Punt_Return_Yards_For_Away_RB1()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[27].PuntReturnStats.Yards;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Punt_Return_Touchdowns_For_Away_RB1()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[27].PuntReturnStats.Touchdowns;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Rush_Attempts_For_Away_RB2()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[28].RushStats.Attempts;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Rush_Yards_For_Away_RB2()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[28].RushStats.Yards;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Rush_Touchdowns_For_Away_RB2()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[28].RushStats.Touchdowns;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Receiptions_For_Away_RB2()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[28].RecStats.Receptions;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Receiving_Yards_For_Away_RB2()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[28].RecStats.Yards;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Receiving_Touchdowns_For_Away_RB2()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[28].RecStats.Touchdowns;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Kick_Returns_For_Away_RB2()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[28].KickReturnStats.Returns;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Kick_Return_Yards_For_Away_RB2()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[28].KickReturnStats.Yards;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Kick_Return_Touchdowns_For_Away_RB2()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[28].KickReturnStats.Touchdowns;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Punt_Returns_For_Away_RB2()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[28].PuntReturnStats.Returns;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Punt_Return_Yards_For_Away_RB2()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[28].PuntReturnStats.Yards;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Punt_Return_Touchdowns_For_Away_RB2()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[28].PuntReturnStats.Touchdowns;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Rush_Attempts_For_Away_RB3()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[29].RushStats.Attempts;

            Assert.AreEqual(1, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Rush_Yards_For_Away_RB3()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[29].RushStats.Yards;

            Assert.AreEqual(14, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Rush_Touchdowns_For_Away_RB3()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[29].RushStats.Touchdowns;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Receiptions_For_Away_RB3()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[29].RecStats.Receptions;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Receiving_Yards_For_Away_RB3()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[29].RecStats.Yards;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Receiving_Touchdowns_For_Away_RB3()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[29].RecStats.Touchdowns;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Kick_Returns_For_Away_RB3()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[29].KickReturnStats.Returns;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Kick_Return_Yards_For_Away_RB3()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[29].KickReturnStats.Yards;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Kick_Return_Touchdowns_For_Away_RB3()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[29].KickReturnStats.Touchdowns;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Punt_Returns_For_Away_RB3()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[29].PuntReturnStats.Returns;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Punt_Return_Yards_For_Away_RB3()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[29].PuntReturnStats.Yards;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Punt_Return_Touchdowns_For_Away_RB3()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[29].PuntReturnStats.Touchdowns;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Rush_Attempts_For_Away_RB4()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[30].RushStats.Attempts;

            Assert.AreEqual(3, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Rush_Yards_For_Away_RB4()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[30].RushStats.Yards;

            Assert.AreEqual(36, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Rush_Touchdowns_For_Away_RB4()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[30].RushStats.Touchdowns;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Receiptions_For_Away_RB4()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[30].RecStats.Receptions;

            Assert.AreEqual(1, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Receiving_Yards_For_Away_RB4()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[30].RecStats.Yards;

            Assert.AreEqual(7, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Receiving_Touchdowns_For_Away_RB4()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[30].RecStats.Touchdowns;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Kick_Returns_For_Away_RB4()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[30].KickReturnStats.Returns;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Kick_Return_Yards_For_Away_RB4()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[30].KickReturnStats.Yards;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Kick_Return_Touchdowns_For_Away_RB4()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[30].KickReturnStats.Touchdowns;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Punt_Returns_For_Away_RB4()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[30].PuntReturnStats.Returns;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Punt_Return_Yards_For_Away_RB4()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[30].PuntReturnStats.Yards;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Punt_Return_Touchdowns_For_Away_RB4()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[30].PuntReturnStats.Touchdowns;

            Assert.AreEqual(0, result);
        }
        #endregion

        #region GetPlayerStats - WR Stats
        [Test]
        public void GetPlayerStats_Should_Get_Correct_Rush_Attempts_For_Home_WR1()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[6].RushStats.Attempts;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Rush_Yards_For_Home_WR1()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[6].RushStats.Yards;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Rush_Touchdowns_For_Home_WR1()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[6].RushStats.Touchdowns;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Receiptions_For_Home_WR1()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[6].RecStats.Receptions;

            Assert.AreEqual(1, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Receiving_Yards_For_Home_WR1()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[6].RecStats.Yards;

            Assert.AreEqual(19, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Receiving_Touchdowns_For_Home_WR1()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[6].RecStats.Touchdowns;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Kick_Returns_For_Home_WR1()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[6].KickReturnStats.Returns;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Kick_Return_Yards_For_Home_WR1()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[6].KickReturnStats.Yards;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Kick_Return_Touchdowns_For_Home_WR1()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[6].KickReturnStats.Touchdowns;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Punt_Returns_For_Home_WR1()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[6].PuntReturnStats.Returns;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Punt_Return_Yards_For_Home_WR1()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[6].PuntReturnStats.Yards;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Punt_Return_Touchdowns_For_Home_WR1()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[6].PuntReturnStats.Touchdowns;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Rush_Attempts_For_Home_WR2()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[7].RushStats.Attempts;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Rush_Yards_For_Home_WR2()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[7].RushStats.Yards;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Rush_Touchdowns_For_Home_WR2()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[7].RushStats.Touchdowns;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Receiptions_For_Home_WR2()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[7].RecStats.Receptions;

            Assert.AreEqual(1, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Receiving_Yards_For_Home_WR2()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[7].RecStats.Yards;

            Assert.AreEqual(38, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Receiving_Touchdowns_For_Home_WR2()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[7].RecStats.Touchdowns;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Kick_Returns_For_Home_WR2()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[7].KickReturnStats.Returns;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Kick_Return_Yards_For_Home_WR2()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[7].KickReturnStats.Yards;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Kick_Return_Touchdowns_For_Home_WR2()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[7].KickReturnStats.Touchdowns;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Punt_Returns_For_Home_WR2()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[7].PuntReturnStats.Returns;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Punt_Return_Yards_For_Home_WR2()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[7].PuntReturnStats.Yards;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Punt_Return_Touchdowns_For_Home_WR2()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[7].PuntReturnStats.Touchdowns;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Rush_Attempts_For_Home_WR3()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[8].RushStats.Attempts;

            Assert.AreEqual(3, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Rush_Yards_For_Home_WR3()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[8].RushStats.Yards;

            Assert.AreEqual(84, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Rush_Touchdowns_For_Home_WR3()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[8].RushStats.Touchdowns;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Receiptions_For_Home_WR3()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[8].RecStats.Receptions;

            Assert.AreEqual(1, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Receiving_Yards_For_Home_WR3()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[8].RecStats.Yards;

            Assert.AreEqual(25, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Receiving_Touchdowns_For_Home_WR3()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[8].RecStats.Touchdowns;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Kick_Returns_For_Home_WR3()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[8].KickReturnStats.Returns;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Kick_Return_Yards_For_Home_WR3()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[8].KickReturnStats.Yards;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Kick_Return_Touchdowns_For_Home_WR3()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[8].KickReturnStats.Touchdowns;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Punt_Returns_For_Home_WR3()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[8].PuntReturnStats.Returns;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Punt_Return_Yards_For_Home_WR3()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[8].PuntReturnStats.Yards;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Punt_Return_Touchdowns_For_Home_WR3()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[8].PuntReturnStats.Touchdowns;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Rush_Attempts_For_Home_WR4()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[9].RushStats.Attempts;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Rush_Yards_For_Home_WR4()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[9].RushStats.Yards;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Rush_Touchdowns_For_Home_WR4()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[9].RushStats.Touchdowns;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Receiptions_For_Home_WR4()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[9].RecStats.Receptions;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Receiving_Yards_For_Home_WR4()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[9].RecStats.Yards;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Receiving_Touchdowns_For_Home_WR4()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[9].RecStats.Touchdowns;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Kick_Returns_For_Home_WR4()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[9].KickReturnStats.Returns;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Kick_Return_Yards_For_Home_WR4()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[9].KickReturnStats.Yards;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Kick_Return_Touchdowns_For_Home_WR4()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[9].KickReturnStats.Touchdowns;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Punt_Returns_For_Home_WR4()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[9].PuntReturnStats.Returns;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Punt_Return_Yards_For_Home_WR4()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[9].PuntReturnStats.Yards;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Punt_Return_Touchdowns_For_Home_WR4()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[9].PuntReturnStats.Touchdowns;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Rush_Attempts_For_Away_WR1()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[31].RushStats.Attempts;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Rush_Yards_For_Away_WR1()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[31].RushStats.Yards;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Rush_Touchdowns_For_Away_WR1()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[31].RushStats.Touchdowns;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Receiptions_For_Away_WR1()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[31].RecStats.Receptions;

            Assert.AreEqual(2, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Receiving_Yards_For_Away_WR1()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[31].RecStats.Yards;

            Assert.AreEqual(70, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Receiving_Touchdowns_For_Away_WR1()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[31].RecStats.Touchdowns;

            Assert.AreEqual(1, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Kick_Returns_For_Away_WR1()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[31].KickReturnStats.Returns;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Kick_Return_Yards_For_Away_WR1()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[31].KickReturnStats.Yards;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Kick_Return_Touchdowns_For_Away_WR1()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[31].KickReturnStats.Touchdowns;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Punt_Returns_For_Away_WR1()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[31].PuntReturnStats.Returns;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Punt_Return_Yards_For_Away_WR1()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[31].PuntReturnStats.Yards;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Punt_Return_Touchdowns_For_Away_WR1()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[31].PuntReturnStats.Touchdowns;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Rush_Attempts_For_Away_WR2()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[32].RushStats.Attempts;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Rush_Yards_For_Away_WR2()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[32].RushStats.Yards;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Rush_Touchdowns_For_Away_WR2()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[32].RushStats.Touchdowns;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Receiptions_For_Away_WR2()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[32].RecStats.Receptions;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Receiving_Yards_For_Away_WR2()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[32].RecStats.Yards;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Receiving_Touchdowns_For_Away_WR2()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[32].RecStats.Touchdowns;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Kick_Returns_For_Away_WR2()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[32].KickReturnStats.Returns;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Kick_Return_Yards_For_Away_WR2()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[32].KickReturnStats.Yards;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Kick_Return_Touchdowns_For_Away_WR2()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[32].KickReturnStats.Touchdowns;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Punt_Returns_For_Away_WR2()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[32].PuntReturnStats.Returns;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Punt_Return_Yards_For_Away_WR2()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[32].PuntReturnStats.Yards;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Punt_Return_Touchdowns_For_Away_WR2()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[32].PuntReturnStats.Touchdowns;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Rush_Attempts_For_Away_WR3()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[33].RushStats.Attempts;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Rush_Yards_For_Away_WR3()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[33].RushStats.Yards;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Rush_Touchdowns_For_Away_WR3()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[33].RushStats.Touchdowns;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Receiptions_For_Away_WR3()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[33].RecStats.Receptions;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Receiving_Yards_For_Away_WR3()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[33].RecStats.Yards;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Receiving_Touchdowns_For_Away_WR3()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[33].RecStats.Touchdowns;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Kick_Returns_For_Away_WR3()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[33].KickReturnStats.Returns;

            Assert.AreEqual(4, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Kick_Return_Yards_For_Away_WR3()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[33].KickReturnStats.Yards;

            Assert.AreEqual(58, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Kick_Return_Touchdowns_For_Away_WR3()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[33].KickReturnStats.Touchdowns;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Punt_Returns_For_Away_WR3()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[33].PuntReturnStats.Returns;

            Assert.AreEqual(1, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Punt_Return_Yards_For_Away_WR3()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[33].PuntReturnStats.Yards;

            Assert.AreEqual(5, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Punt_Return_Touchdowns_For_Away_WR3()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[33].PuntReturnStats.Touchdowns;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Rush_Attempts_For_Away_WR4()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[34].RushStats.Attempts;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Rush_Yards_For_Away_WR4()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[34].RushStats.Yards;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Rush_Touchdowns_For_Away_WR4()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[34].RushStats.Touchdowns;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Receiptions_For_Away_WR4()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[34].RecStats.Receptions;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Receiving_Yards_For_Away_WR4()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[34].RecStats.Yards;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Receiving_Touchdowns_For_Away_WR4()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[34].RecStats.Touchdowns;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Kick_Returns_For_Away_WR4()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[34].KickReturnStats.Returns;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Kick_Return_Yards_For_Away_WR4()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[34].KickReturnStats.Yards;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Kick_Return_Touchdowns_For_Away_WR4()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[34].KickReturnStats.Touchdowns;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Punt_Returns_For_Away_WR4()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[34].PuntReturnStats.Returns;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Punt_Return_Yards_For_Away_WR4()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[34].PuntReturnStats.Yards;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Punt_Return_Touchdowns_For_Away_WR4()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[34].PuntReturnStats.Touchdowns;

            Assert.AreEqual(0, result);
        }
        #endregion

        #region GetPlayerStats - TE Stats
        [Test]
        public void GetPlayerStats_Should_Get_Correct_Rush_Attempts_For_Home_TE1()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[10].RushStats.Attempts;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Rush_Yards_For_Home_TE1()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[10].RushStats.Yards;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Rush_Touchdowns_For_Home_TE1()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[10].RushStats.Touchdowns;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Receiptions_For_Home_TE1()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[10].RecStats.Receptions;

            Assert.AreEqual(1, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Receiving_Yards_For_Home_TE1()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[10].RecStats.Yards;

            Assert.AreEqual(10, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Receiving_Touchdowns_For_Home_TE1()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[10].RecStats.Touchdowns;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Kick_Returns_For_Home_TE1()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[10].KickReturnStats.Returns;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Kick_Return_Yards_For_Home_TE1()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[10].KickReturnStats.Yards;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Kick_Return_Touchdowns_For_Home_TE1()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[10].KickReturnStats.Touchdowns;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Punt_Returns_For_Home_TE1()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[10].PuntReturnStats.Returns;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Punt_Return_Yards_For_Home_TE1()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[10].PuntReturnStats.Yards;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Punt_Return_Touchdowns_For_Home_TE1()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[10].PuntReturnStats.Touchdowns;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Rush_Attempts_For_Home_TE2()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[11].RushStats.Attempts;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Rush_Yards_For_Home_TE2()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[11].RushStats.Yards;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Rush_Touchdowns_For_Home_TE2()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[11].RushStats.Touchdowns;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Receiptions_For_Home_TE2()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[11].RecStats.Receptions;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Receiving_Yards_For_Home_TE2()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[11].RecStats.Yards;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Receiving_Touchdowns_For_Home_TE2()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[11].RecStats.Touchdowns;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Kick_Returns_For_Home_TE2()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[11].KickReturnStats.Returns;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Kick_Return_Yards_For_Home_TE2()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[11].KickReturnStats.Yards;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Kick_Return_Touchdowns_For_Home_TE2()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[11].KickReturnStats.Touchdowns;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Punt_Returns_For_Home_TE2()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[11].PuntReturnStats.Returns;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Punt_Return_Yards_For_Home_TE2()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[11].PuntReturnStats.Yards;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Punt_Return_Touchdowns_For_Home_TE2()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[11].PuntReturnStats.Touchdowns;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Rush_Attempts_For_Away_TE1()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[35].RushStats.Attempts;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Rush_Yards_For_Away_TE1()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[35].RushStats.Yards;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Rush_Touchdowns_For_Away_TE1()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[35].RushStats.Touchdowns;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Receiptions_For_Away_TE1()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[35].RecStats.Receptions;

            Assert.AreEqual(1, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Receiving_Yards_For_Away_TE1()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[35].RecStats.Yards;

            Assert.AreEqual(17, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Receiving_Touchdowns_For_Away_TE1()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[35].RecStats.Touchdowns;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Kick_Returns_For_Away_TE1()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[35].KickReturnStats.Returns;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Kick_Return_Yards_For_Away_TE1()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[35].KickReturnStats.Yards;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Kick_Return_Touchdowns_For_Away_TE1()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[35].KickReturnStats.Touchdowns;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Punt_Returns_For_Away_TE1()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[35].PuntReturnStats.Returns;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Punt_Return_Yards_For_Away_TE1()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[35].PuntReturnStats.Yards;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Punt_Return_Touchdowns_For_Away_TE1()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[35].PuntReturnStats.Touchdowns;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Rush_Attempts_For_Away_TE2()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[36].RushStats.Attempts;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Rush_Yards_For_Away_TE2()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[36].RushStats.Yards;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Rush_Touchdowns_For_Away_TE2()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[36].RushStats.Touchdowns;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Receiptions_For_Away_TE2()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[36].RecStats.Receptions;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Receiving_Yards_For_Away_TE2()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[36].RecStats.Yards;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Receiving_Touchdowns_For_Away_TE2()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[36].RecStats.Touchdowns;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Kick_Returns_For_Away_TE2()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[36].KickReturnStats.Returns;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Kick_Return_Yards_For_Away_TE2()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[36].KickReturnStats.Yards;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Kick_Return_Touchdowns_For_Away_TE2()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[36].KickReturnStats.Touchdowns;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Punt_Returns_For_Away_TE2()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[36].PuntReturnStats.Returns;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Punt_Return_Yards_For_Away_TE2()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[36].PuntReturnStats.Yards;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Punt_Return_Touchdowns_For_Away_TE2()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[36].PuntReturnStats.Touchdowns;

            Assert.AreEqual(0, result);
        }
        #endregion

        #region GetPlayerStats - DEF Stats
        [Test]
        public void GetPlayerStats_Should_Get_Correct_Sacks_For_Home_DL1()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[12].DefStats.Sacks;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Interceptions_For_Home_DL1()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[12].DefStats.Interceptions;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Interception_Yards_For_Home_DL1()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[12].DefStats.Yards;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Interception_Touchdowns_For_Home_DL1()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[12].DefStats.Touchdowns;

            Assert.AreEqual(0, result);
        }

        public void GetPlayerStats_Should_Get_Correct_Sacks_For_Home_DL2()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[13].DefStats.Sacks;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Interceptions_For_Home_DL2()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[13].DefStats.Interceptions;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Interception_Yards_For_Home_DL2()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[13].DefStats.Yards;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Interception_Touchdowns_For_Home_DL2()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[13].DefStats.Touchdowns;

            Assert.AreEqual(0, result);
        }

        public void GetPlayerStats_Should_Get_Correct_Sacks_For_Home_DL3()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[14].DefStats.Sacks;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Interceptions_For_Home_DL3()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[14].DefStats.Interceptions;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Interception_Yards_For_Home_DL3()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[14].DefStats.Yards;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Interception_Touchdowns_For_Home_DL3()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[14].DefStats.Touchdowns;

            Assert.AreEqual(0, result);
        }

        public void GetPlayerStats_Should_Get_Correct_Sacks_For_Home_LB1()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[15].DefStats.Sacks;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Interceptions_For_Home_LB1()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[15].DefStats.Interceptions;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Interception_Yards_For_Home_LB1()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[15].DefStats.Yards;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Interception_Touchdowns_For_Home_LB1()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[15].DefStats.Touchdowns;

            Assert.AreEqual(0, result);
        }

        public void GetPlayerStats_Should_Get_Correct_Sacks_For_Home_LB2()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[16].DefStats.Sacks;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Interceptions_For_Home_LB2()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[16].DefStats.Interceptions;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Interception_Yards_For_Home_LB2()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[16].DefStats.Yards;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Interception_Touchdowns_For_Home_LB2()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[16].DefStats.Touchdowns;

            Assert.AreEqual(0, result);
        }

        public void GetPlayerStats_Should_Get_Correct_Sacks_For_Home_LB3()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[17].DefStats.Sacks;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Interceptions_For_Home_LB3()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[17].DefStats.Interceptions;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Interception_Yards_For_Home_LB3()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[17].DefStats.Yards;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Interception_Touchdowns_For_Home_LB3()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[17].DefStats.Touchdowns;

            Assert.AreEqual(0, result);
        }

        public void GetPlayerStats_Should_Get_Correct_Sacks_For_Home_LB4()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[18].DefStats.Sacks;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Interceptions_For_Home_LB4()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[18].DefStats.Interceptions;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Interception_Yards_For_Home_LB4()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[18].DefStats.Yards;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Interception_Touchdowns_For_Home_LB4()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[18].DefStats.Touchdowns;

            Assert.AreEqual(0, result);
        }

        public void GetPlayerStats_Should_Get_Correct_Sacks_For_Home_DB1()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[19].DefStats.Sacks;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Interceptions_For_Home_DB1()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[19].DefStats.Interceptions;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Interception_Yards_For_Home_DB1()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[19].DefStats.Yards;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Interception_Touchdowns_For_Home_DB1()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[19].DefStats.Touchdowns;

            Assert.AreEqual(0, result);
        }

        public void GetPlayerStats_Should_Get_Correct_Sacks_For_Home_DB2()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[20].DefStats.Sacks;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Interceptions_For_Home_DB2()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[20].DefStats.Interceptions;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Interception_Yards_For_Home_DB2()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[20].DefStats.Yards;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Interception_Touchdowns_For_Home_DB2()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[20].DefStats.Touchdowns;

            Assert.AreEqual(0, result);
        }

        public void GetPlayerStats_Should_Get_Correct_Sacks_For_Home_DB3()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[21].DefStats.Sacks;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Interceptions_For_Home_DB3()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[21].DefStats.Interceptions;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Interception_Yards_For_Home_DB3()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[21].DefStats.Yards;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Interception_Touchdowns_For_Home_DB3()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[21].DefStats.Touchdowns;

            Assert.AreEqual(0, result);
        }

        public void GetPlayerStats_Should_Get_Correct_Sacks_For_Home_DB4()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[22].DefStats.Sacks;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Interceptions_For_Home_DB4()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[22].DefStats.Interceptions;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Interception_Yards_For_Home_DB4()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[22].DefStats.Yards;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Interception_Touchdowns_For_Home_DB4()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[22].DefStats.Touchdowns;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Sacks_For_Away_DL1()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[37].DefStats.Sacks;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Interceptions_For_Away_DL1()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[37].DefStats.Interceptions;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Interception_Yards_For_Away_DL1()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[37].DefStats.Yards;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Interception_Touchdowns_For_Away_DL1()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[37].DefStats.Touchdowns;

            Assert.AreEqual(0, result);
        }

        public void GetPlayerStats_Should_Get_Correct_Sacks_For_Away_DL2()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[38].DefStats.Sacks;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Interceptions_For_Away_DL2()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[38].DefStats.Interceptions;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Interception_Yards_For_Away_DL2()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[38].DefStats.Yards;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Interception_Touchdowns_For_Away_DL2()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[38].DefStats.Touchdowns;

            Assert.AreEqual(0, result);
        }

        public void GetPlayerStats_Should_Get_Correct_Sacks_For_Away_DL3()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[39].DefStats.Sacks;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Interceptions_For_Away_DL3()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[39].DefStats.Interceptions;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Interception_Yards_For_Away_DL3()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[39].DefStats.Yards;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Interception_Touchdowns_For_Away_DL3()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[39].DefStats.Touchdowns;

            Assert.AreEqual(0, result);
        }

        public void GetPlayerStats_Should_Get_Correct_Sacks_For_Away_LB1()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[40].DefStats.Sacks;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Interceptions_For_Away_LB1()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[40].DefStats.Interceptions;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Interception_Yards_For_Away_LB1()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[40].DefStats.Yards;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Interception_Touchdowns_For_Away_LB1()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[40].DefStats.Touchdowns;

            Assert.AreEqual(0, result);
        }

        public void GetPlayerStats_Should_Get_Correct_Sacks_For_Away_LB2()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[41].DefStats.Sacks;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Interceptions_For_Away_LB2()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[41].DefStats.Interceptions;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Interception_Yards_For_Away_LB2()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[41].DefStats.Yards;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Interception_Touchdowns_For_Away_LB2()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[41].DefStats.Touchdowns;

            Assert.AreEqual(0, result);
        }

        public void GetPlayerStats_Should_Get_Correct_Sacks_For_Away_LB3()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[42].DefStats.Sacks;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Interceptions_For_Away_LB3()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[42].DefStats.Interceptions;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Interception_Yards_For_Away_LB3()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[42].DefStats.Yards;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Interception_Touchdowns_For_Away_LB3()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[42].DefStats.Touchdowns;

            Assert.AreEqual(0, result);
        }

        public void GetPlayerStats_Should_Get_Correct_Sacks_For_Away_LB4()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[43].DefStats.Sacks;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Interceptions_For_Away_LB4()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[43].DefStats.Interceptions;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Interception_Yards_For_Away_LB4()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[43].DefStats.Yards;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Interception_Touchdowns_For_Away_LB4()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[43].DefStats.Touchdowns;

            Assert.AreEqual(0, result);
        }

        public void GetPlayerStats_Should_Get_Correct_Sacks_For_Away_DB1()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[44].DefStats.Sacks;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Interceptions_For_Away_DB1()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[44].DefStats.Interceptions;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Interception_Yards_For_Away_DB1()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[44].DefStats.Yards;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Interception_Touchdowns_For_Away_DB1()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[44].DefStats.Touchdowns;

            Assert.AreEqual(0, result);
        }

        public void GetPlayerStats_Should_Get_Correct_Sacks_For_Away_DB2()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[45].DefStats.Sacks;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Interceptions_For_Away_DB2()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[45].DefStats.Interceptions;

            Assert.AreEqual(1, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Interception_Yards_For_Away_DB2()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[45].DefStats.Yards;

            Assert.AreEqual(-1, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Interception_Touchdowns_For_Away_DB2()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[45].DefStats.Touchdowns;

            Assert.AreEqual(0, result);
        }

        public void GetPlayerStats_Should_Get_Correct_Sacks_For_Away_DB3()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[46].DefStats.Sacks;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Interceptions_For_Away_DB3()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[46].DefStats.Interceptions;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Interception_Yards_For_Away_DB3()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[46].DefStats.Yards;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Interception_Touchdowns_For_Away_DB3()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[46].DefStats.Touchdowns;

            Assert.AreEqual(0, result);
        }

        public void GetPlayerStats_Should_Get_Correct_Sacks_For_Away_DB4()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[47].DefStats.Sacks;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Interceptions_For_Away_DB4()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[47].DefStats.Interceptions;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Interception_Yards_For_Away_DB4()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[47].DefStats.Yards;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Interception_Touchdowns_For_Away_DB4()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[47].DefStats.Touchdowns;

            Assert.AreEqual(0, result);
        }
        #endregion

        #region GetPlayerStats - K Stats
        [Test]
        public void GetPlayerStats_Should_Get_Correct_Field_Goal_Attempts_For_Home_Kicker()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[23].KickStats.FieldGoalAttempts;

            Assert.AreEqual(2, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Field_Goals_Made_For_Home_Kicker()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[23].KickStats.FieldGoalsMade;

            Assert.AreEqual(2, result);
        }

        public void GetPlayerStats_Should_Get_Correct_Extra_Points_Attempts_For_Home_Kicker()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[23].KickStats.ExtraPointAttempts;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Extra_Points_Made_For_Home_Kicker()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[23].KickStats.ExtraPointsMade;

            Assert.AreEqual(1, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Field_Goal_Attempts_For_Away_Kicker()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[48].KickStats.FieldGoalAttempts;

            Assert.AreEqual(2, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Field_Goals_Made_For_Away_Kicker()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[48].KickStats.FieldGoalsMade;

            Assert.AreEqual(2, result);
        }

        public void GetPlayerStats_Should_Get_Correct_Extra_Points_Attempts_For_Away_Kicker()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[48].KickStats.ExtraPointAttempts;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Extra_Points_Made_For_Away_Kicker()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[48].KickStats.ExtraPointsMade;

            Assert.AreEqual(1, result);
        }
        #endregion

        #region GetPlayerStats - P Stats
        [Test]
        public void GetPlayerStats_Should_Get_Correct_Punts_For_Home_Punter()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[24].PuntStats.Punts;

            Assert.AreEqual(1, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Punt_Yards_For_Home_Punter()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[24].PuntStats.Yards;

            Assert.AreEqual(69, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Punts_For_Away_Punter()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[49].PuntStats.Punts;

            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPlayerStats_Should_Get_Correct_Punt_Yards_For_Away_Punter()
        {
            var playerStats = this.nestopiaExtractor.GetPlayerStats(this.overtimeDecisionSaveStateBytes).ToList();
            var result = playerStats[49].PuntStats.Yards;

            Assert.AreEqual(0, result);
        }
        #endregion

        #region GetTeamStats
        [Test]
        public void GetTeamStats_Should_Get_Correct_TeamId_For_Home_Team()
        {
            const int homeTeamId = 1;
            var result = this.nestopiaExtractor.GetTeamStats(this.saveStateBytes);

            var homeTeam = result.First(p => p.IsHome);
            Assert.AreEqual(homeTeamId, homeTeam.TeamId);
        }

        [Test]
        public void GetTeamStats_Should_Get_Correct_OpposingTeamId_For_Home_Team()
        {
            const int awayTeamId = 27;
            var result = this.nestopiaExtractor.GetTeamStats(this.saveStateBytes);

            var homeTeam = result.First(p => p.IsHome);
            Assert.AreEqual(awayTeamId, homeTeam.OpposingTeamId);
        }

        [Test]
        public void GetTeamStats_Should_Get_Correct_TeamId_For_Away_Team()
        {
            const int awayTeamId = 27;
            var result = this.nestopiaExtractor.GetTeamStats(this.saveStateBytes);

            var awayTeam = result.First(p => !p.IsHome);
            Assert.AreEqual(awayTeamId, awayTeam.TeamId);
        }

        [Test]
        public void GetTeamStats_Should_Get_Correct_OpposingTeamId_For_Away_Team()
        {
            const int homeTeamId = 1;
            var result = this.nestopiaExtractor.GetTeamStats(this.saveStateBytes);

            var awayTeam = result.First(p => !p.IsHome);
            Assert.AreEqual(homeTeamId, awayTeam.OpposingTeamId);
        }

        [Test]
        public void GetTeamStats_Should_Get_Correct_FirstDowns_For_Home_Team()
        {
            const int homeFirstDowns = 8;
            var result = this.nestopiaExtractor.GetTeamStats(this.saveStateBytes);

            var homeTeam = result.First(p => p.IsHome);
            Assert.AreEqual(homeFirstDowns, homeTeam.FirstDowns);
        }

        [Test]
        public void GetTeamStats_Should_Get_Correct_FirstDowns_For_Away_Team()
        {
            const int awayFirstDowns = 7;
            var result = this.nestopiaExtractor.GetTeamStats(this.saveStateBytes);

            var awayTeam = result.First(p => !p.IsHome);
            Assert.AreEqual(awayFirstDowns, awayTeam.FirstDowns);
        }

        [Test]
        public void GetTeamStats_Should_Get_Correct_IsOvertime_For_Home_Team()
        {
            var result = this.nestopiaExtractor.GetTeamStats(this.saveStateBytes);

            var homeTeam = result.First(p => p.IsHome);
            Assert.False(homeTeam.IsOvertime);
        }

        [Test]
        public void GetTeamStats_Should_Get_Correct_ScoreFirstQuarter_For_Home_Team()
        {
            const int scoreFirstQuarter = 7;
            var result = this.nestopiaExtractor.GetTeamStats(this.saveStateBytes);

            var homeTeam = result.First(p => p.IsHome);
            Assert.AreEqual(scoreFirstQuarter, homeTeam.ScoreFirstQuarter);
        }

        [Test]
        public void GetTeamStats_Should_Get_Correct_ScoreSecondQuarter_For_Home_Team()
        {
            const int scoreSecondQuarter = 7;
            var result = this.nestopiaExtractor.GetTeamStats(this.saveStateBytes);

            var homeTeam = result.First(p => p.IsHome);
            Assert.AreEqual(scoreSecondQuarter, homeTeam.ScoreSecondQuarter);
        }

        [Test]
        public void GetTeamStats_Should_Get_Correct_ScoreThirdQuarter_For_Home_Team()
        {
            const int scoreThirdQuarter = 0;
            var result = this.nestopiaExtractor.GetTeamStats(this.saveStateBytes);

            var homeTeam = result.First(p => p.IsHome);
            Assert.AreEqual(scoreThirdQuarter, homeTeam.ScoreThirdQuarter);
        }

        [Test]
        public void GetTeamStats_Should_Get_Correct_ScoreFourthQuarter_For_Home_Team()
        {
            const int scoreFourthQuarter = 0;
            var result = this.nestopiaExtractor.GetTeamStats(this.saveStateBytes);

            var homeTeam = result.First(p => p.IsHome);
            Assert.AreEqual(scoreFourthQuarter, homeTeam.ScoreFourthQuarter);
        }

        [Test]
        public void GetTeamStats_Should_Get_Correct_ScoreFinal_For_Home_Team()
        {
            const int scoreFinal = 14;
            var result = this.nestopiaExtractor.GetTeamStats(this.saveStateBytes);

            var homeTeam = result.First(p => p.IsHome);
            Assert.AreEqual(scoreFinal, homeTeam.ScoreFinal);
        }

        [Test]
        public void GetTeamStats_Should_Get_Correct_Decision_For_Home_Team()
        {
            var result = this.nestopiaExtractor.GetTeamStats(this.saveStateBytes);

            var homeTeam = result.First(p => p.IsHome);
            Assert.AreEqual(Decision.Loss, homeTeam.Decision);
        }

        [Test]
        public void GetTeamStats_Should_Get_Correct_IsOvertime_For_Away_Team()
        {
            var result = this.nestopiaExtractor.GetTeamStats(this.saveStateBytes);

            var awayTeam = result.First(p => !p.IsHome);
            Assert.False(awayTeam.IsOvertime);
        }

        [Test]
        public void GetTeamStats_Should_Get_Correct_ScoreFirstQuarter_For_Away_Team()
        {
            const int scoreFirstQuarter = 7;
            var result = this.nestopiaExtractor.GetTeamStats(this.saveStateBytes);

            var awayTeam = result.First(p => !p.IsHome);
            Assert.AreEqual(scoreFirstQuarter, awayTeam.ScoreFirstQuarter);
        }

        [Test]
        public void GetTeamStats_Should_Get_Correct_ScoreSecondQuarter_For_Away_Team()
        {
            const int scoreSecondQuarter = 10;
            var result = this.nestopiaExtractor.GetTeamStats(this.saveStateBytes);

            var awayTeam = result.First(p => !p.IsHome);
            Assert.AreEqual(scoreSecondQuarter, awayTeam.ScoreSecondQuarter);
        }

        [Test]
        public void GetTeamStats_Should_Get_Correct_ScoreThirdQuarter_For_Away_Team()
        {
            const int scoreThirdQuarter = 0;
            var result = this.nestopiaExtractor.GetTeamStats(this.saveStateBytes);

            var awayTeam = result.First(p => !p.IsHome);
            Assert.AreEqual(scoreThirdQuarter, awayTeam.ScoreThirdQuarter);
        }

        [Test]
        public void GetTeamStats_Should_Get_Correct_ScoreFourthQuarter_For_Away_Team()
        {
            const int scoreFourthQuarter = 10;
            var result = this.nestopiaExtractor.GetTeamStats(this.saveStateBytes);

            var awayTeam = result.First(p => !p.IsHome);
            Assert.AreEqual(scoreFourthQuarter, awayTeam.ScoreFourthQuarter);
        }

        [Test]
        public void GetTeamStats_Should_Get_Correct_ScoreFinal_For_Away_Team()
        {
            const int scoreFinal = 27;
            var result = this.nestopiaExtractor.GetTeamStats(this.saveStateBytes);

            var awayTeam = result.First(p => !p.IsHome);
            Assert.AreEqual(scoreFinal, awayTeam.ScoreFinal);
        }

        [Test]
        public void GetTeamStats_Should_Get_Correct_Decision_For_Away_Team()
        {
            var result = this.nestopiaExtractor.GetTeamStats(this.saveStateBytes);

            var awayTeam = result.First(p => !p.IsHome);
            Assert.AreEqual(Decision.Win, awayTeam.Decision);
        }

        [Test]
        public void GetTeamStats_Should_Get_Correct_IsOvertime_For_Home_Team_For_Overtime_Game()
        {
            var result = this.nestopiaExtractor.GetTeamStats(this.overtimeDecisionSaveStateBytes);

            var homeTeam = result.First(p => p.IsHome);
            Assert.True(homeTeam.IsOvertime);
        }

        [Test]
        public void GetTeamStats_Should_Get_Correct_ScoreFirstQuarter_For_Home_Team_For_Overtime_Game()
        {
            const double scoreFirstQuarter = 3.25;
            var result = this.nestopiaExtractor.GetTeamStats(this.overtimeDecisionSaveStateBytes);

            var homeTeam = result.First(p => p.IsHome);
            Assert.AreEqual(scoreFirstQuarter, homeTeam.ScoreFirstQuarter);
        }

        [Test]
        public void GetTeamStats_Should_Get_Correct_ScoreSecondQuarter_For_Home_Team_For_Overtime_Game()
        {
            const double scoreSecondQuarter = 3.25;
            var result = this.nestopiaExtractor.GetTeamStats(this.overtimeDecisionSaveStateBytes);

            var homeTeam = result.First(p => p.IsHome);
            Assert.AreEqual(scoreSecondQuarter, homeTeam.ScoreSecondQuarter);
        }

        [Test]
        public void GetTeamStats_Should_Get_Correct_ScoreThirdQuarter_For_Home_Team_For_Overtime_Game()
        {
            const double scoreThirdQuarter = 3.25;
            var result = this.nestopiaExtractor.GetTeamStats(this.overtimeDecisionSaveStateBytes);

            var homeTeam = result.First(p => p.IsHome);
            Assert.AreEqual(scoreThirdQuarter, homeTeam.ScoreThirdQuarter);
        }

        [Test]
        public void GetTeamStats_Should_Get_Correct_ScoreFourthQuarter_For_Home_Team_For_Overtime_Game()
        {
            const double scoreFourthQuarter = 3.25;
            var result = this.nestopiaExtractor.GetTeamStats(this.overtimeDecisionSaveStateBytes);

            var homeTeam = result.First(p => p.IsHome);
            Assert.AreEqual(scoreFourthQuarter, homeTeam.ScoreFourthQuarter);
        }

        [Test]
        public void GetTeamStats_Should_Get_Correct_ScoreFinal_For_Home_Team_For_Overtime_Game()
        {
            const int scoreFinal = 19;
            var result = this.nestopiaExtractor.GetTeamStats(this.overtimeDecisionSaveStateBytes);

            var homeTeam = result.First(p => p.IsHome);
            Assert.AreEqual(scoreFinal, homeTeam.ScoreFinal);
        }

        [Test]
        public void GetTeamStats_Should_Get_Correct_Decision_For_Home_Team_For_Overtime_Game()
        {
            var result = this.nestopiaExtractor.GetTeamStats(this.overtimeDecisionSaveStateBytes);

            var homeTeam = result.First(p => p.IsHome);
            Assert.AreEqual(Decision.Win, homeTeam.Decision);
        }

        [Test]
        public void GetTeamStats_Should_Get_Correct_IsOvertime_For_Away_Team_For_Overtime_Game()
        {
            var result = this.nestopiaExtractor.GetTeamStats(this.overtimeDecisionSaveStateBytes);

            var awayTeam = result.First(p => !p.IsHome);
            Assert.True(awayTeam.IsOvertime);
        }

        [Test]
        public void GetTeamStats_Should_Get_Correct_ScoreFirstQuarter_For_Away_Team_For_Overtime_Game()
        {
            const double scoreFirstQuarter = 3.25;
            var result = this.nestopiaExtractor.GetTeamStats(this.overtimeDecisionSaveStateBytes);

            var awayTeam = result.First(p => !p.IsHome);
            Assert.AreEqual(scoreFirstQuarter, awayTeam.ScoreFirstQuarter);
        }

        [Test]
        public void GetTeamStats_Should_Get_Correct_ScoreSecondQuarter_For_Away_Team_For_Overtime_Game()
        {
            const double scoreSecondQuarter = 3.25;
            var result = this.nestopiaExtractor.GetTeamStats(this.overtimeDecisionSaveStateBytes);

            var awayTeam = result.First(p => !p.IsHome);
            Assert.AreEqual(scoreSecondQuarter, awayTeam.ScoreSecondQuarter);
        }

        [Test]
        public void GetTeamStats_Should_Get_Correct_ScoreThirdQuarter_For_Away_Team_For_Overtime_Game()
        {
            const double scoreThirdQuarter = 3.25;
            var result = this.nestopiaExtractor.GetTeamStats(this.overtimeDecisionSaveStateBytes);

            var awayTeam = result.First(p => !p.IsHome);
            Assert.AreEqual(scoreThirdQuarter, awayTeam.ScoreThirdQuarter);
        }

        [Test]
        public void GetTeamStats_Should_Get_Correct_ScoreFourthQuarter_For_Away_Team_For_Overtime_Game()
        {
            const double scoreFourthQuarter = 3.25;
            var result = this.nestopiaExtractor.GetTeamStats(this.overtimeDecisionSaveStateBytes);

            var awayTeam = result.First(p => !p.IsHome);
            Assert.AreEqual(scoreFourthQuarter, awayTeam.ScoreFourthQuarter);
        }

        [Test]
        public void GetTeamStats_Should_Get_Correct_ScoreFinal_For_Away_Team_For_Overtime_Game()
        {
            const int scoreFinal = 13;
            var result = this.nestopiaExtractor.GetTeamStats(this.overtimeDecisionSaveStateBytes);

            var awayTeam = result.First(p => !p.IsHome);
            Assert.AreEqual(scoreFinal, awayTeam.ScoreFinal);
        }

        [Test]
        public void GetTeamStats_Should_Get_Correct_Decision_For_Away_Team_For_Overtime_Game()
        {
            var result = this.nestopiaExtractor.GetTeamStats(this.overtimeDecisionSaveStateBytes);

            var awayTeam = result.First(p => !p.IsHome);
            Assert.AreEqual(Decision.Loss, awayTeam.Decision);
        }

        [Test]
        public void GetTeamStats_Should_Get_Correct_Decision_For_Home_Team_For_Overtime_Game_Tie()
        {
            var result = this.nestopiaExtractor.GetTeamStats(this.overtimeTieSaveStateBytes);

            var homeTeam = result.First(p => p.IsHome);
            Assert.AreEqual(Decision.Tie, homeTeam.Decision);
        }

        [Test]
        public void GetTeamStats_Should_Get_Correct_Decision_For_Away_Team_For_Overtime_Game_Tie()
        {
            var result = this.nestopiaExtractor.GetTeamStats(this.overtimeTieSaveStateBytes);

            var awayTeam = result.First(p => !p.IsHome);
            Assert.AreEqual(Decision.Tie, awayTeam.Decision);
        }
        #endregion

        private static byte[] GetSaveStateBytes(string filePath)
        {
            return File.ReadAllBytes(filePath);
        }
    }
}