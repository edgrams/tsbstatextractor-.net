﻿using NUnit.Framework;
using TSBStatExtractor.Constants;
using TSBStatExtractor.Exceptions;
using TSBStatExtractor.Extractors.TSBOriginal;
using TSBStatExtractor.Factories;

namespace TSBStatExtractor.Tests.Factories
{
    [TestFixture]
    public class ExtractorFactoryTests
    {
        private ExtractorFactory extractorFactory;

        [SetUp]
        public void SetUp()
        {
            this.extractorFactory = new ExtractorFactory();
        }

        [Test]
        public void Build_Should_Return_TSBOriginalNesticleExtractor_Instance_When_Save_State_Length_Equals_Nesticle_SaveStateLength()
        {
            var result = this.extractorFactory.Build(SaveStateLengths.Nesticle);

            Assert.IsInstanceOf<TSBOriginalNesticleExtractor>(result);
        }

        [Test]
        public void Build_Should_Return_TSBOriginalNestopiaExtractor_Instance_When_Save_State_Length_Equals_Nesticle_SaveStateLength()
        {
            var result = this.extractorFactory.Build(SaveStateLengths.Nestopia);

            Assert.IsInstanceOf<TSBOriginalNestopiaExtractor>(result);
        }

        [Test]
        public void Build_Should_Throw_When_Emulator_Equals_Unknown()
        {
            Assert.Throws<TSBStatExtractorException>(() => this.extractorFactory.Build(0));
        }
    }
}