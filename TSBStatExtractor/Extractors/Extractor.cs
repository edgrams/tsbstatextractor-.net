﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Linq;
using TSBDomain.Enums;
using TSBDomain.Models;
using TSBDomain.Models.Player.Stats;
using TSBDomain.Models.Team.Stats;
using TSBStatExtractor.Exceptions;
using TSBStatExtractor.Interfaces;
using TSBStatExtractor.Resources;

namespace TSBStatExtractor.Extractors
{
    abstract class Extractor : IExtractor
    {
        protected abstract int SaveStateLength { get; }

        protected abstract int PlayerStatsLocation { get; }

        protected abstract int TeamStatsLocation { get; }

        protected abstract int ScoresLocation { get; }

        protected abstract int FirstDownsLocation { get; }

        protected abstract int NumberOfPlayers { get; }

        protected abstract int NumberOfOffensivePlayers { get; }

        protected abstract int NumberOfDefensivePlayers { get; }

        protected abstract void AddFirstDowns(BinaryReader binaryReader, List<TeamStats> teamStats);

        protected abstract void AddScores(BinaryReader binaryReader, List<TeamStats> teamStats);

        protected abstract void MapTeamScores(List<TeamStats> teamStats, IList<byte> teamScores, bool isOvertime, bool isHome);

        /// <summary>
        /// Gets the game stats.
        /// </summary>
        /// <param name="saveState">State of the save.</param>
        /// <returns></returns>
        public Game GetGameStats(byte[] saveState)
        {
            this.VerifySaveState(saveState);

            var teamStats = this.GetTeamStats(saveState);
            var playerStats = this.GetPlayerStats(saveState);

            this.MapPlayerStatsToTeamStats(teamStats, playerStats);

            return Game.Create(teamStats);
        }

        [ExcludeFromCodeCoverage]
        internal void MapPlayerStatsToTeamStats(List<TeamStats> teamStats,
            List<PlayerStats> playerStats)
        {
            var homeTeamPlayers = playerStats.Take(this.NumberOfPlayers).ToList();
            this.MapPlayerId(homeTeamPlayers, teamStats[0].TeamId);
            teamStats[0].PlayerStats = homeTeamPlayers;

            var awayTeamPlayers = playerStats.Skip(this.NumberOfPlayers)
                .Take(NumberOfPlayers).ToList();
            this.MapPlayerId(awayTeamPlayers, teamStats[1].TeamId);
            teamStats[1].PlayerStats = awayTeamPlayers;
        }

        internal void MapPlayerId(List<PlayerStats> playerStats, int teamId)
        {
            var playerStart = teamId * this.NumberOfPlayers;
            for (var i = 0; i < playerStats.Count(); i++)
            {
                playerStats[i].PlayerId = Convert.ToInt16((i + 1) + playerStart);
            }
        }

        internal List<TeamStats> GetTeamStats(byte[] saveState)
        {
            var teamStats = CreateTeamStats();
            return this.PopuluteTeamStats(saveState, teamStats);
        }

        internal static List<TeamStats> CreateTeamStats()
        {
            var homeTeamStats = TeamStats.Create(true);
            var awayTeamStats = TeamStats.Create(false);
            return new List<TeamStats> { homeTeamStats, awayTeamStats };
        }

        [ExcludeFromCodeCoverage]
        private List<TeamStats> PopuluteTeamStats(byte[] saveState, List<TeamStats> teamStats)
        {
            var binaryReader = new BinaryReader(new MemoryStream(saveState));
            this.AddTeamIds(binaryReader, teamStats);
            this.AddFirstDowns(binaryReader, teamStats);
            this.AddScores(binaryReader, teamStats);
            return teamStats;
        }

        [ExcludeFromCodeCoverage]
        private void AddTeamIds(BinaryReader binaryReader, List<TeamStats> teamStats)
        {
            binaryReader.BaseStream.Seek(this.TeamStatsLocation, SeekOrigin.Begin);
            var homeTeamId = binaryReader.ReadByte();
            var awayTeamId = binaryReader.ReadByte();
            teamStats.First(p => p.IsHome).TeamId = homeTeamId;
            teamStats.First(p => p.IsHome).OpposingTeamId = awayTeamId;
            teamStats.First(p => !p.IsHome).TeamId = awayTeamId;
            teamStats.First(p => !p.IsHome).OpposingTeamId = homeTeamId;
        }

        [ExcludeFromCodeCoverage]
        internal static void MapDecision(List<TeamStats> teamStats, int homeTeamScore, int awayTeamScore)
        {
            teamStats.First(p => p.IsHome).Decision = GetDecision(homeTeamScore, awayTeamScore);
            teamStats.First(p => !p.IsHome).Decision = GetDecision(awayTeamScore, homeTeamScore);
        }

        internal static Decision GetDecision(int score, int opposingScore)
        {
            if (score > opposingScore)
            {
                return Decision.Win;
            }

            if (score < opposingScore)
            {
                return Decision.Loss;
            }

            return Decision.Tie;
        }

        internal List<PlayerStats> GetPlayerStats(byte[] saveState)
        {
            var binaryReader = new BinaryReader(new MemoryStream(saveState));
            binaryReader.BaseStream.Seek(this.PlayerStatsLocation, SeekOrigin.Begin);

            var playerStats = new List<PlayerStats>();

            // for each team (2 teams)
            for (var i = 0; i <= 1; i++)
            {
                var teamPlayerStats = new List<PlayerStats>();

                // qb player stats
                var qbPlayerStats = GetQBPlayerStats(binaryReader);
                teamPlayerStats.AddRange(qbPlayerStats);

                // off player stats
                var offPlayerStats = GetOffPlayerStats(binaryReader);
                teamPlayerStats.AddRange(offPlayerStats);

                // def player stats
                var defPlayerStats = GetDefPlayerStats(binaryReader);
                teamPlayerStats.AddRange(defPlayerStats);

                // kicker stats
                var kickerStats = GetKickerStats(binaryReader);
                teamPlayerStats.Add(kickerStats);

                // punter stats
                var punterStats = GetPunterStats(binaryReader);
                teamPlayerStats.Add(punterStats);

                // skip 8 bytes!
                binaryReader.BaseStream.Seek(8, SeekOrigin.Current);

                // players health
                var health = GetPlayersHealth(binaryReader);

                // player conditions
                var conditions = GetPlayerConditions(binaryReader);

                MapPlayerStats(teamPlayerStats, health, conditions);
                playerStats.AddRange(teamPlayerStats);
            }
            return playerStats;
        }

        [ExcludeFromCodeCoverage]
        private void MapPlayerStats(List<PlayerStats> teamPlayerStats,
            List<Health> teamPlayersHealth, List<Condition> teamPlayerConditions)
        {
            for (var i = 0; i < this.NumberOfOffensivePlayers; i++)
            {
                teamPlayerStats[i].Health = teamPlayersHealth[i];
            }

            for (var i = 0; i < this.NumberOfPlayers; i++)
            {
                teamPlayerStats[i].Condition = teamPlayerConditions[i];
            }
        }

        [ExcludeFromCodeCoverage]
        private static IEnumerable<PlayerStats> GetQBPlayerStats(BinaryReader binaryReader)
        {
            var qbPlayerStatsCollection = new List<PlayerStats>();
            for (var i = 0; i <= 1; i++)
            {
                // pass stats
                var passAttempts = binaryReader.ReadByte();
                var passCompletions = binaryReader.ReadByte();
                var passTouchdowns = binaryReader.ReadByte();
                var passInterceptions = binaryReader.ReadByte();
                var passYards = binaryReader.ReadByte();
                var passYardsMultiplier = binaryReader.ReadByte();
                var passStats = PassStats.Create(passAttempts, passCompletions,
                    GetYards(passYards, passYardsMultiplier), passTouchdowns, passInterceptions);

                // rush stats
                var rushAttempts = binaryReader.ReadByte();
                var rushYards = binaryReader.ReadByte();
                var rushYardsMultiplier = binaryReader.ReadByte();
                var rushTouchdowns = binaryReader.ReadByte();
                var rushStats = RushStats.Create(rushAttempts,
                    GetYards(rushYards, rushYardsMultiplier), rushTouchdowns);

                var qbPlayerStats = new PlayerStats
                {
                    PassStats = passStats,
                    RushStats = rushStats
                };
                qbPlayerStatsCollection.Add(qbPlayerStats);
            }
            return qbPlayerStatsCollection;
        }

        [ExcludeFromCodeCoverage]
        private IEnumerable<PlayerStats> GetOffPlayerStats(BinaryReader binaryReader)
        {
            var offPlayerStatsCollection = new List<PlayerStats>();
            // account for qbs (off - 2)
            for (var i = 0; i < (this.NumberOfOffensivePlayers - 2); i++)
            {
                // rec stats
                var receptions = binaryReader.ReadByte();
                var recYards = binaryReader.ReadByte();
                var recYardsMultiplier = binaryReader.ReadByte();
                var recTouchdowns = binaryReader.ReadByte();
                var recStats = RecStats.Create(receptions,
                    GetYards(recYards, recYardsMultiplier), recTouchdowns);

                // kick return stats
                var kickReturns = binaryReader.ReadByte();
                var kickReturnYards = binaryReader.ReadByte();
                var kickReturnYardsMultiplier = binaryReader.ReadByte();
                var kickReturnTouchdowns = binaryReader.ReadByte();
                var kickReturnStats = KickReturnStats.Create(kickReturns,
                    GetYards(kickReturnYards, kickReturnYardsMultiplier), kickReturnTouchdowns);

                // punt return stats
                var puntReturns = binaryReader.ReadByte();
                var puntReturnYards = binaryReader.ReadByte();
                var puntReturnYardsMultiplier = binaryReader.ReadByte();
                var puntReturnTouchdowns = binaryReader.ReadByte();
                var puntReturnStats = PuntReturnStats.Create(puntReturns,
                    GetYards(puntReturnYards, puntReturnYardsMultiplier), puntReturnTouchdowns);

                // rush stats
                var rushAttempts = binaryReader.ReadByte();
                var rushYards = binaryReader.ReadByte();
                var rushYardsMultiplier = binaryReader.ReadByte();
                var rushTouchdowns = binaryReader.ReadByte();
                var rushStats = RushStats.Create(rushAttempts,
                    GetYards(rushYards, rushYardsMultiplier), rushTouchdowns);

                var offPlayerStats = new PlayerStats
                {
                    RushStats = rushStats,
                    RecStats = recStats,
                    KickReturnStats = kickReturnStats,
                    PuntReturnStats = puntReturnStats
                };
                offPlayerStatsCollection.Add(offPlayerStats);
            }
            return offPlayerStatsCollection;
        }

        [ExcludeFromCodeCoverage]
        private IEnumerable<PlayerStats> GetDefPlayerStats(BinaryReader binaryReader)
        {
            var defPlayerStatsCollection = new List<PlayerStats>();
            for (var i = 0; i < this.NumberOfDefensivePlayers; i++)
            {
                // def stats
                var sacks = binaryReader.ReadByte();
                var interceptions = binaryReader.ReadByte();
                var intYards = binaryReader.ReadByte();
                var intYardsMultiplier = binaryReader.ReadByte();
                var intTouchdowns = binaryReader.ReadByte();
                var defStats = DefStats.Create(sacks, interceptions,
                    GetYards(intYards, intYardsMultiplier), intTouchdowns);

                var defPlayerStats = new PlayerStats
                {
                    DefStats = defStats
                };
                defPlayerStatsCollection.Add(defPlayerStats);
            }
            return defPlayerStatsCollection;
        }

        [ExcludeFromCodeCoverage]
        private static PlayerStats GetKickerStats(BinaryReader binaryReader)
        {
            var extraPointAttempts = binaryReader.ReadByte();
            var extraPointsMade = binaryReader.ReadByte();
            var fieldGoalAttempts = binaryReader.ReadByte();
            var fieldGoalsMade = binaryReader.ReadByte();
            var kickStats = KickStats.Create(extraPointsMade, extraPointAttempts,
                fieldGoalsMade, fieldGoalAttempts);

            return new PlayerStats
            {
                KickStats = kickStats
            };
        }

        [ExcludeFromCodeCoverage]
        private static PlayerStats GetPunterStats(BinaryReader binaryReader)
        {
            var punts = binaryReader.ReadByte();
            var puntYards = binaryReader.ReadByte();
            var puntYardsMultiplier = binaryReader.ReadByte();
            var puntStats = PuntStats.Create(punts, GetYards(puntYards, puntYardsMultiplier));

            return new PlayerStats
            {
                PuntStats = puntStats
            };
        }

        [ExcludeFromCodeCoverage]
        private List<Health> GetPlayersHealth(BinaryReader binaryReader)
        {
            var playersHealthCollection = new List<Health>();
            var playersHealth = "";
            for (var i = 0; i <= 2; i++)
            {
                var temp = Convert.ToString(binaryReader.ReadByte(), 2);
                for (var t = temp.Length; t <= 7; t++)
                {
                    temp = "0" + temp;
                }
                playersHealth += temp;
            }

            for (var i = 0; i < this.NumberOfOffensivePlayers; i++)
            {
                var healthValue = playersHealth.Substring(i * 2, 2);
                var health = (Health) Convert.ToInt32(healthValue, 2);
                playersHealthCollection.Add(health);
            }

            return playersHealthCollection;
        }

        [ExcludeFromCodeCoverage]
        private List<Condition> GetPlayerConditions(BinaryReader binaryReader)
        {
            var conditionCollection = new List<Condition>();

            var conditions = "";
            for (var i = 0; i <= 7; i++)
            {
                var temp = Convert.ToString(binaryReader.ReadByte(), 2);
                for (var t = temp.Length; t <= 7; t++)
                {
                    temp = "0" + temp;
                }
                conditions += temp;
            }

            for (var i = 0; i < this.NumberOfPlayers; i++)
            {
                var conditionValue = conditions.Substring(i * 2, 2);
                var condition = (Condition) Convert.ToByte(conditionValue, 2);
                conditionCollection.Add(condition);
            }
            return conditionCollection;
        }

        internal static Int16 GetYards(byte yards, byte multiplier)
        {
            // arbitrary high number for multiplier, need to distinguish positive / negative yards
            if (multiplier < 10)
            {
                return Convert.ToInt16((yards + (multiplier * 256)));
            }
            return Convert.ToInt16((-(256 - yards) - ((255 - multiplier) * 256)));
        }

        protected void VerifySaveState(byte[] saveState)
        {
            if (saveState == null)
            {
                throw new ArgumentNullException("saveState");
            }

            if (saveState.Length != this.SaveStateLength)
            {
                throw new TSBStatExtractorException(ExceptionMessages.SaveStateFileTypeUnknown);
            }
        }
    }
}