﻿using TSBStatExtractor.Constants;

namespace TSBStatExtractor.Extractors.TSBOriginal
{
    class TSBOriginalNestopiaExtractor : TSBOriginalExtractor
    {
        protected override int SaveStateLength
        {
            get { return SaveStateLengths.Nestopia; }
        }

        protected override int PlayerStatsLocation
        {
            get { return 5781; }
        }

        protected override int TeamStatsLocation
        {
            get { return 164; }
        }

        protected override int ScoresLocation
        {
            get { return 973; }
        }

        protected override int FirstDownsLocation
        {
            get { return 6429; }
        }
    }
}