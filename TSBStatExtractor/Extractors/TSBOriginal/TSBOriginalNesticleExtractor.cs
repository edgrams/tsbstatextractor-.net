﻿using System;
using TSBStatExtractor.Constants;

namespace TSBStatExtractor.Extractors.TSBOriginal
{
    class TSBOriginalNesticleExtractor : TSBOriginalExtractor
    {
        protected override int SaveStateLength
        {
            get { return SaveStateLengths.Nesticle; }
        }

        protected override int PlayerStatsLocation
        {
            get { throw new NotImplementedException(); }
        }

        protected override int TeamStatsLocation
        {
            get { throw new NotImplementedException(); }
        }

        protected override int ScoresLocation
        {
            get { throw new NotImplementedException(); }
        }

        protected override int FirstDownsLocation
        {
            get { throw new NotImplementedException(); }
        }
    }
}