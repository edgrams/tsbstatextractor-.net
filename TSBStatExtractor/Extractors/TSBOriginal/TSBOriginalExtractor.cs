﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Linq;
using TSBDomain.Models.Team.Stats;

namespace TSBStatExtractor.Extractors.TSBOriginal
{
    abstract class TSBOriginalExtractor : Extractor
    {
        protected override int NumberOfPlayers
        {
            get { return 25; }
        }

        protected override int NumberOfOffensivePlayers
        {
            get { return 12; }
        }

        protected override int NumberOfDefensivePlayers
        {
            get { return 11; }
        }

        [ExcludeFromCodeCoverage]
        protected override void AddFirstDowns(BinaryReader binaryReader, List<TeamStats> teamStats)
        {
            binaryReader.BaseStream.Seek(this.FirstDownsLocation, SeekOrigin.Begin);
            var homeFirstDowns = binaryReader.ReadByte();
            var awayFirstDowns = binaryReader.ReadByte();
            teamStats.First(p => p.IsHome).FirstDowns = homeFirstDowns;
            teamStats.First(p => !p.IsHome).FirstDowns = awayFirstDowns;
        }

        [ExcludeFromCodeCoverage]
        protected override void AddScores(BinaryReader binaryReader, List<TeamStats> teamStats)
        {
            binaryReader.BaseStream.Seek(this.ScoresLocation, SeekOrigin.Begin);

            var homeFinalScore = 0;
            var homeTeamScores = new byte[5];
            for (var i = 0; i <= 3; i++)
            {
                var score = (byte)Int32.Parse(Convert.ToString(binaryReader.ReadByte(), 16));
                homeTeamScores[i] = score;
                homeFinalScore += score;
            }
            homeTeamScores[4] = (byte)Int32.Parse(Convert.ToString(binaryReader.ReadByte(), 16));
            var isOvertime = homeFinalScore != homeTeamScores[4];

            var awayTeamScores = new byte[5];
            for (var i = 0; i <= 4; i++)
            {
                awayTeamScores[i] = (byte)Int32.Parse(Convert.ToString(binaryReader.ReadByte(), 16));
            }

            MapTeamScores(teamStats, homeTeamScores, isOvertime, true);
            MapTeamScores(teamStats, awayTeamScores, isOvertime, false);
            MapDecision(teamStats, homeTeamScores[4], awayTeamScores[4]);
        }

        [ExcludeFromCodeCoverage]
        protected override void MapTeamScores(List<TeamStats> teamStats, IList<byte> teamScores, bool isOvertime, bool isHome)
        {
            teamStats.First(p => p.IsHome == isHome).IsOvertime = isOvertime;
            teamStats.First(p => p.IsHome == isHome).ScoreFirstQuarter = GetScore(teamScores, isOvertime, 1);
            teamStats.First(p => p.IsHome == isHome).ScoreSecondQuarter = GetScore(teamScores, isOvertime, 2);
            teamStats.First(p => p.IsHome == isHome).ScoreThirdQuarter = GetScore(teamScores, isOvertime, 3);
            teamStats.First(p => p.IsHome == isHome).ScoreFourthQuarter = GetScore(teamScores, isOvertime, 4);
            teamStats.First(p => p.IsHome == isHome).ScoreOvertimeQuarter = (byte) GetScore(teamScores, isOvertime, 5);
            teamStats.First(p => p.IsHome == isHome).ScoreFinal = teamScores[4];
        }

        internal static double GetScore(IList<byte> scores, bool isOvertime, int quarter)
        {
            if (quarter == 5)
            {
                return isOvertime ? scores[0] : 0;
            }

            if (isOvertime)
            {
                // subtract first overtime score from final score
                double regulationScore = scores[4] - scores[0];
                return regulationScore / 4;
            }
            return scores[quarter - 1];
        }
    }
}