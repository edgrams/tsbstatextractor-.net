﻿using TSBStatExtractor.Constants;
using TSBStatExtractor.Exceptions;
using TSBStatExtractor.Extractors.TSBOriginal;
using TSBStatExtractor.Interfaces;
using TSBStatExtractor.Resources;

namespace TSBStatExtractor.Factories
{
    class ExtractorFactory : IExtractorFactory
    {
        /// <summary>
        /// Builds the specified save state length.
        /// </summary>
        /// <param name="saveStateLength">Length of the save state.</param>
        /// <returns></returns>
        /// <exception cref="TSBStatExtractorException"></exception>
        public IExtractor Build(int saveStateLength)
        {
            IExtractor extractor;
            switch (saveStateLength)
            {
                case SaveStateLengths.Nesticle:
                    extractor = new TSBOriginalNesticleExtractor();
                    break;
                case SaveStateLengths.Nestopia:
                    extractor = new TSBOriginalNestopiaExtractor();
                    break;
                default:
                    throw new TSBStatExtractorException(ExceptionMessages.SaveStateFileTypeUnknown);
            }
            return extractor;
        }
    }
}