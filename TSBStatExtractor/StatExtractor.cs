﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using Ninject;
using Ninject.Modules;
using TSBStatExtractor.Exceptions;
using TSBStatExtractor.Interfaces;
using TSBStatExtractor.Modules;
using TSBStatExtractor.Resources;
using TSBDomain.Models;

namespace TSBStatExtractor
{
    public class StatExtractor : IStatExtractor
    {
        private IKernel kernel;

        protected StatExtractor() { }

        /// <summary>
        /// Creates this instance.
        /// </summary>
        /// <returns></returns>
        public static IStatExtractor Create()
        {
            return new StatExtractor();
        }

        /// <summary>
        /// Defines the entry point of the application.
        /// </summary>
        public static void Main()
        {
            // for testing
            var statExtractor = Create();
            var gameStats = statExtractor.ExtractFromFile(
                "C:\\Users\\Ed\\Desktop\\Nestopia140bin\\states\\tpc_original.ns1");
        }

        /// <summary>
        /// Extracts from file.
        /// </summary>
        /// <param name="filePath">The file path.</param>
        /// <returns></returns>
        /// <exception cref="System.ArgumentNullException">filePath</exception>
        /// <exception cref="TSBStatExtractorException"></exception>
        public Game ExtractFromFile(string filePath)
        {
            if (string.IsNullOrEmpty(filePath))
            {
                throw new ArgumentNullException("filePath");
            }

            byte[] saveState;
            try
            {
                saveState = File.ReadAllBytes(filePath);
            }
            catch (FileNotFoundException)
            {
                var exceptionMessage = string.Format(ExceptionMessages.SaveStateFileDoesNotExist, filePath);
                throw new TSBStatExtractorException(exceptionMessage);
            }

            return this.Extract(saveState);
        }

        /// <summary>
        /// Extracts the specified save state.
        /// </summary>
        /// <param name="saveState">State of the save.</param>
        /// <returns></returns>
        /// <exception cref="System.ArgumentNullException">saveState</exception>
        public Game Extract(byte[] saveState)
        {
            if (saveState == null)
            {
                throw new ArgumentNullException("saveState");
            }

            this.LoadModules();

            var extractorFactory = this.GetExtractorFactory();
            var extractor = extractorFactory.Build(saveState.Length);
            return extractor.GetGameStats(saveState);
        }

        [ExcludeFromCodeCoverage]
        internal virtual void LoadModules()
        {
            var modules = new INinjectModule[] { new TSBStatExtractorModule() };
            kernel = new StandardKernel(modules);
        }

        [ExcludeFromCodeCoverage]
        internal virtual IExtractorFactory GetExtractorFactory()
        {
            return kernel.Get<IExtractorFactory>();
        }
    }
}
