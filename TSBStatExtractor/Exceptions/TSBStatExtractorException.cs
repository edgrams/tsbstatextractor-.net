﻿using TSBDomain.Exceptions;

namespace TSBStatExtractor.Exceptions
{
    public class TSBStatExtractorException : TSBException
    {
        public TSBStatExtractorException(string message)
            : base(message) { }
    }
}
