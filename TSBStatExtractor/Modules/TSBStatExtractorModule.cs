﻿using Ninject.Modules;
using TSBStatExtractor.Factories;
using TSBStatExtractor.Interfaces;

namespace TSBStatExtractor.Modules
{
    class TSBStatExtractorModule : NinjectModule
    {
        public override void Load()
        {
            Bind<IExtractorFactory>().To<ExtractorFactory>();
        }
    }
}