﻿using TSBDomain.Models;

namespace TSBStatExtractor.Interfaces
{
    public interface IStatExtractor
    {
        /// <summary>
        /// Extracts the specified save state.
        /// </summary>
        /// <param name="saveState">State of the save.</param>
        /// <returns></returns>
        Game Extract(byte[] saveState);

        /// <summary>
        /// Extracts from file.
        /// </summary>
        /// <param name="filePath">The file path.</param>
        /// <returns></returns>
        Game ExtractFromFile(string filePath);
    }
}