﻿using TSBDomain.Models;
namespace TSBStatExtractor.Interfaces
{
    public interface IExtractor
    {
        /// <summary>
        /// Gets the game stats.
        /// </summary>
        /// <param name="saveState">State of the save.</param>
        /// <returns></returns>
        Game GetGameStats(byte[] saveState);
    }
}
