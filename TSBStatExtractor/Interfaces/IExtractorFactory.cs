﻿
namespace TSBStatExtractor.Interfaces
{
    interface IExtractorFactory
    {
        /// <summary>
        /// Builds the specified save state length.
        /// </summary>
        /// <param name="saveStateLength">Length of the save state.</param>
        /// <returns></returns>
        IExtractor Build(int saveStateLength);
    }
}